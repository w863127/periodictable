function getSite(folderName, pageName) {
    const site = `<iframe width="100%" src="${folderName}/${pageName}.html" id="mainframe" onload="SetCwinHeight()" marginheight="0" scrolling="No" frameborder="No"><iframe>`;
    const content = document.getElementById('content');
    content.innerHTML = site;
}

function showHome() {
    document.getElementById('content').innerHTML = 'Hello World';
}

//處理點擊事件
function handleClickEvent(){
    if(this.id === 'homeBtn'){
        showHome();
    }else{
        getSite(this.className, this.id);
    }
}

function init() {
    //目錄監聽事件
    const subconten = document.querySelectorAll('a');
    for(let idList of subconten){
        document.getElementById(idList.id).onclick = handleClickEvent;
    }

    // for (let i = 0; i < subconten.length; i += 1) {
    //     console.log(subconten[i]);
    //     document.getElementById(subconten[i]).onclick = handleClickEvent;
    // }
    // const subconten = document.getElementsByClassName('subnav-content');
    // console.log(subconten.getId());
    // for (let i = 0; i < subconten.length; i += 1) {
    //     for (let j = 0; j < subconten[i].children.length; j += 1) {
    //         let divId = subconten[i].children[j].id;
    //         document.getElementById(divId).onclick = handleClickEvent;
    //     }
    // }

}

function SetCwinHeight() {
    const iframeid = document.getElementById('mainframe'); // iframe id
    console.log(iframeid);
    if (document.getElementById) {
        console.log('C');
        if (iframeid && !window.opera) {
            console.log('D');
            if (iframeid.contentDocument && iframeid.contentDocument.body.offsetHeight) {
                console.log('A');
                iframeid.height = iframeid.contentDocument.body.offsetHeight + 100;
                console.log(iframeid.height);
            } else if (iframeid.Document && iframeid.Document.body.scrollHeight) {
                console.log('B');
                iframeid.height = iframeid.Document.body.scrollHeight + 10;
                console.log(iframeid.height);
            }
        }
    }
}

window.addEventListener('load', init());
