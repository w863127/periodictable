/**
 * 創建String物件
 */
function differentDefineString() {
    const strA = '123';
    const strB = '123';
    const strOA = new String('123');
    const strOB = new String('123');

    console.log(strA == strB); // true
    console.log(strA === strB); // true
    console.log(strA == strOA); // true
    console.log(strA === strOA); // false
    console.log(strOA == strOB); // false
    console.log(strOA === strOB); // false

    /** NOTE
     * strA建立，建立字串類型物件，內容123
     * strB建立，指向123
     * strOA建立，建立字串類型物件，內容123
     * strOB建立，建立字串類型物件，內容123
     * 物件A不等於物件B
     */
}

/** 創建物件
 * factory function
*/
function factoryModel() {
    const s = Symbol('hello world');
    console.log(typeof s); // symbol
    console.log(s); // Symbol(hello world)
}

function iteratorWithArray() {
    const myArray = [1, 2, 3, 4, 5];
    const iterator = myArray[Symbol.iterator]();

    console.log(iterator.next()); // {value: 1, done: false}
    console.log(iterator.next()); // {value: 2, done: false}
    console.log(iterator.next()); // {value: 3, done: false}
    console.log(iterator.next()); // {value: 4, done: false}
    console.log(iterator.next()); // {value: 5, done: false}
    console.log(iterator.next()); // {value: undefined, done: true}

    // 使用iterator
    const [first, ...otherNum] = myArray;
    console.log(first); // 1
    console.log(otherNum); // [2,3,4,5]
    console.log(myArray[first]); // 2 相當於 myArray[1]

    // 物件要實作@@uteratir method，即物件需有Symbol.iterator屬性
    // Array & TypeArray
    // String
    // Map
    // Set
    // arguments
    // DOM Elements
}

function exeState(state) {
    let str = '';
    let done = false;

    if (!state.step) {
        state.step = 0;
    }
    switch (state.step) {
    case 0:
        str = 'hello';
        break;
    case 1:
        str = 'world';
        break;
    default:
        done = true;
        break;
    }
    if (!done) {
        ++state.step;
    }
    return { done, value: str };
}

function* hello_g() {
    yield 'hello'; // 第0次執行
    yield 'world'; // 第1次執行
}

function useGenerator() {
    const state = {};
    console.log(exeState(state)); // {done: false, value: "hello"}
    console.log(exeState(state)); // {done: false, value: "world"}
    console.log(exeState(state)); // {done: true, value: ""}

    const iter = hello_g();
    console.log(iter.next()); // {value: "hello", done: false}
    console.log(iter.next()); // {value: "world", done: false}
    console.log(iter.next()); // {value: undefined, done: true}
}

function whatReflect() {
    const myObject = {
        a: 1,
        b: 2,
        get cul() {
            return this.a + this.b;
        },
    };

    const myReceiverObject = {
        a: 1,
        b: 2,
    };
    console.log(Reflect.get(myObject, 'cul', myReceiverObject)); // 3

    console.log('assign' in Object); // true
    Reflect.has(Object, 'assign'); // true
}

function init() {
    /**
     * Jacascript 型態
     * Underfined
     * Null
     * Boolean
     * Number
     * String
     * Object
     * Symbol --not a constructor
     */
    differentDefineString();
    factoryModel();
    iteratorWithArray();
    useGenerator();
    whatReflect();
}
