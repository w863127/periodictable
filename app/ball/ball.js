// 設定canvas
const canvas = document.querySelector('canvas');
canvas.width = window.width;
canvas.height = window.height;
const ctx = canvas.getContext('2d');

const { width } = canvas; // const width = canvas.width;
const { height } = canvas; // const height = canvas.height;

// 亂數
function random(min, max) {
    let num = Math.floor(Math.random() * (max - min)) + min;
    return num;
}

function Shape(x, y, velX, velY, exits) {
    this.x = x;
    this.y = y;
    this.velX = velX;
    this.velY = velY;
    this.exits = exits;
}

// 定義球
function Ball(x, y, velX, velY, exits, color, size) {
    // 連接Shape
    Shape.call(x, y, velX, velY, exits);
    this.color = color;
    this.size = size;
}
Ball.prototype = new Shape();
Ball.prototype.constructor = Ball();

// 畫球
Ball.prototype.draw = function () {
    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
    ctx.fill();
};

// 定義黑洞
function EvilCircle(x, y, exits) {
    Shape.call(this, x, y, 20, 20, exits);
    this.color = 'white';
    this.size = 10;
}


// define ball update method

Ball.prototype.update = function () {
    if ((this.x + this.size) >= width) {
        this.velX = -(this.velX);
    }

    if ((this.x - this.size) <= 0) {
        this.velX = -(this.velX);
    }

    if ((this.y + this.size) >= height) {
        this.velY = -(this.velY);
    }

    if ((this.y - this.size) <= 0) {
        this.velY = -(this.velY);
    }

    this.x += this.velX;
    this.y += this.velY;
};

// define ball collision detection

Ball.prototype.collisionDetect = function () {
    for (let j = 0; j < balls.length; j++) {
        if (!(this === balls[j])) {
            let dx = this.x - balls[j].x;
            let dy = this.y - balls[j].y;
            let distance = Math.sqrt(dx * dx + dy * dy);

            if (distance < this.size + balls[j].size) {
                balls[j].color = this.color = `rgb(${  random(0, 255)  },${  random(0, 255)  },${  random(0, 255) })`;
            }
        }
    }
};

// define array to store balls and populate it

var balls = [];

while (balls.length < 25) {
    let size = random(10, 20);
    let ball = new Ball(
    // ball position always drawn at least one ball width
    // away from the adge of the canvas, to avoid drawing errors
        random(0 + size, width - size),
        random(0 + size, height - size),
        random(-7, 7),
        random(-7, 7),
        `rgb(${  random(0, 255)  },${  random(0, 255)  },${  random(0, 255) })`,
        size,
    );
    balls.push(ball);
}

// define loop that keeps drawing the scene constantly

function loop() {
    ctx.fillStyle = 'rgba(0,0,0,0.25)';
    ctx.fillRect(0, 0, width, height);

    for (let i = 0; i < balls.length; i++) {
        balls[i].draw();
        balls[i].update();
        balls[i].collisionDetect();
    }
    // 刷新動畫
    requestAnimationFrame(loop);
}


loop();
