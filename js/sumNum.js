
/* eslint-env jquery */
function isNum(id, inputText) {
    let result = false;
    const errorMessage = '請輸入數字';
    if (inputText === '' || isNaN(inputText) || inputText.indexOf(' ') > -1) {
        $(`#${id}`).addClass('invalidInput');
        $(`#msg_${id}`).text(errorMessage);
        $(`#msg_${id}`).addClass('invalidText');
        result = true;
    } else {
        $(`#${id}`).removeClass('invalidInput');
        $(`#msg_${id}`).text('');
        $(`#msg_${id}`).removeClass('invalidText');
    }
    return result;
}

// 處理內容變更事件
function handleChangeEvent() {
    // const inputText = $(`#${this.id}`).val();
    const numberArr = [];
    const addedArr = [];
    let allinput = true;
    let targetNum;
    // isNum(this.id, inputText);

    const inputContent = document.querySelectorAll('input');
    for (const idList of inputContent) {
        const inputId = idList.id;
        const inputValue = $(`#${inputId}`).val();
        if (isNum(inputId, inputValue)) {
            allinput = false;
            break;
        } else if (inputId != 'targetNum') {
            numberArr.push(parseFloat(inputValue));
        } else {
            targetNum = parseFloat(inputValue);
        }
    }
    // 全部填寫且合法
    let result = '';
    if (allinput) {
        while (numberArr.length > 0) {
            const addnum = numberArr.shift(); // 被加數
            const addend = targetNum - addnum; // 加數
            if (!addedArr.includes(addnum)) {
                addedArr.push(addnum);
                if (numberArr.includes(addend)) {
                    result += `<p> ${addnum} + ${addend} = ${targetNum} </p>`;
                }
            }
        }
        if(result==''){
            result = '無組合';
        }
        document.getElementById('result').innerHTML = result;


        // for (let i = 0; i < numberArr.length; i += 1) {
        //     const addnum = numberArr[i]; // 被加數
        //     const addend = targetNum - addnum; // 加數
        //     if (!addedMap.has(i)) {
        //         addedMap[i] = addnum;

        //         // if()
        //     }
        // }
        // console.log(addedMap);

        // for (const addnum of numberArr) {
        //     const addend = targetNum - addnum; // 加數
        //     if (!addedArr.includes(addnum)) {
        //         addedArr.push(addnum);
        //         console.log(`addedArr=>${addedArr}`);

        //         console.log(numberArr.includes(addend) && !addedArr.includes(addend));
        //         if (numberArr.includes(addend) && !addedArr.includes(addend)) {
        //             addedArr.push(addend);
        //             console.log(`numberArr=>${numberArr}`);
        //             console.log(`addedArr=>${addedArr}`);
        //             $('#result').text(`${addnum} + ${addend} = ${targetNum} <br>`);
        //         }
        //     }
        // }
    }
}


function init() {
    // 目錄監聽事件
    const inputContent = document.querySelectorAll('input');
    for (const idList of inputContent) {
        document.getElementById(idList.id).onkeyup = handleChangeEvent;
    }
}

window.addEventListener('load', init());
