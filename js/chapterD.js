// 建立物件
const student = new Object();
student.sid = '0000123';
student.name = 'Satoshi';
student.level = 7;

const studentB ={ 
    sid: '0000456',
    name: 'Ali Ababwa',
    level: 6,
    cloth: {
        size: 'M',
        style: 'male',
    },
}

student.sid = '0000123';
student.name = 'Satoshi';
student.level = '7';

function classA() {
    document.getElementById('ex_1-1').innerHTML = `student.sid = ${student.sid}`;
    document.getElementById('ex_1-2').innerHTML = `student[\'sid\'] =` + student['sid'];
    let myPropertyName = 'sid';
    document.getElementById('ex_1-3').innerHTML = `student[${myPropertyName}] =${student[myPropertyName]}`;
    myPropertyName = 'name';
    document.getElementById('ex_1-4').innerHTML = `student[${myPropertyName}] =${student[myPropertyName]}`;

    for (let i in student) {
        document.getElementById('ex_1-5').innerHTML += `student[${i}] =${student[i]} <br>`;
    }
}

function classB() {
    for (let i in student) {
        document.getElementById('ex_2-1').innerHTML = 'student中的屬性: ';
        document.getElementById('ex_2-1').innerHTML += `student[${i}] `;
    }
    document.getElementById('ex_2-2').innerHTML = `student中的屬性Object.keys(student): ${Object.keys(student)}`;
    document.getElementById('ex_2-3').innerHTML = `student中的屬性Object.getOwnPropertyNames(student): ${Object.getOwnPropertyNames(student)}`;
}


function init() {
    classA();
    classB();
}
window.addEventListener('load', init());



//https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Guide/Working_with_Objects
//constuctor  -getter setter is constructor?
//attribute vs property
//prototype like class  || object like class