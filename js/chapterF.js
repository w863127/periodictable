
function parent() {
    this.firstname = 'peter';
}

function employee(name, dept) {
    this.name = name || '';
    this.dept = dept || 'IT';
}


function manager() {
    this.report = [];
}
manager.prototype = new employee();


function init() {
    console.log(new parent());  //parent {firstname: "peter"}
    console.log(new parent);    //parent {firstname: "peter"}

    //還沒new之前name不存在
    console.log(new parent().firstname); //peter
    // console.log(new (parent().firstname)); //Uncaught TypeError: Cannot read property 'name' of undefined

    //parent.firstname 是 underfined, new underfined會報錯
    console.log((new parent).firstname);    //peter
    console.log(parent.firstname);      //undefined
    // console.log(new parent.firstname);    //Uncaught TypeError: parent.name is not a constructor

    /*-------------------- */
    console.log("--------------------------------");
    let m = new manager();
    console.log(m);
    console.log(m.dept);  //IT

}


