function equaltest() {
    let x;
    const y = 1;
    // document.write(typeof(x));
    document.getElementById('test_xy').innerHTML = `x,y=>${x === y}`;
    document.getElementById('test_false').innerHTML = `false=>${false === false}`;
    document.getElementById('test_underfined').innerHTML = `undefined=>${undefined === undefined}`;
    document.getElementById('test_null').innerHTML = `null=>${null === null}`;
    document.getElementById('test_zero').innerHTML = `0=>${0 === 0}`;
    document.getElementById('test_nan').innerHTML = `NaN=>${NaN === NaN}`;
    document.getElementById('test_empty').innerHTML = 'empty string=>' + `${'' === ''}`;
}

function switchtest() {
    const x = 'c';
    switch (x) {
    case 'a':
        console.log(' this is a ');
    case 'b':
        console.log(' this is b ');
    case 'c':
        console.log(' this is c ');
    default:
        console.log(' not a, b, c ');
    }
}

function test() {
    const x = 2;
    const y = 3;
    const z = x + y;
    return z;
}
