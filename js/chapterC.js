const borsize = 9;
function getNineA() {
    let printNine = '<div class="table">';
    for (let i = 1; i <= borsize; i += 1) {
        printNine += '<div class="tr">';
        for (let j = 1; j <= borsize; j += 1) {
            printNine += `<div class="td">${i} x ${j} = ${i * j}</div>`;
        }
        printNine += '</div>';
    }
    printNine += '</div>';
    document.getElementById('nineA').innerHTML = printNine;
}

function getNineB() {
    let printNine = '<div class="table">';
    for (let i = 1; i <= borsize; i += 1) {
        printNine += '<div class="tr">';
        for (let j = 1; j <= i; j += 1) {
            printNine += `<div class="td">${i} x ${j} = ${i * j}</div>`;
        }
        printNine += '</div>';
    }
    printNine += '</div>';
    document.getElementById('nineB').innerHTML = printNine;
}

function getNineC() {
    let printNine = '<div class="table">';
    for (let i = borsize; i >= 1; i -= 1) {
        printNine += '<div class="tr">';
        for (let j = 1; j <= i; j += 1) {
            printNine += `<div class="td">${j} x ${i} = ${i * j}</div>`;
        }
        printNine += '</div>';
    }
    printNine += '</div>';
    document.getElementById('nineC').innerHTML = printNine;
}

function getNineD() {
    let printNine = '<div class="table">';
    for (let i = borsize; i >= 1; i -= 1) {
        printNine += '<div class="tr">';
        for (let j = i; j >= 1; j -= 1) {
            printNine += '<div class="td"></div>';
        }
        for (let j = i; j <= borsize; j += 1) {
            printNine += `<div class="td">${j} x ${i} = ${i * j}</div>`;
        }
        printNine += '</div>';
    }
    printNine += '</div>';
    document.getElementById('nineD').innerHTML = printNine;
}

function getNineE() {
    let printNine = '<div>';
    // let printNine = '<div class="table">';
    for (let i = borsize; i >= 1; i -= 1) {
        let cxt = '<div class="tr">*';
        for (let j = 1; j <= (borsize - 2); j += 1) {
            if (i % borsize === 1) {
                cxt += '*';
            } else {
                cxt += '&nbsp;';
            }
        }
        printNine += `*${cxt}</div><tr>`;
        printNine += '</div>';
    }
    printNine += '</div>';
    document.getElementById('nineD').innerHTML = printNine;
}


function getNineF() {
    let printNine = '';
    for (let i = borsize; i >= 1; i -= 1) {
        for (let j = i; j <= borsize; j += 1) {
            let z;
            if (i * j < 10) {
                z = ` ${i * j}`;
            } else {
                z = `${i * j}`;
            }
            printNine += `${j} x ${i} = ${z} `;
        }
        printNine += '\n';
    }
    console.log(printNine);
}

function init() {
    getNineA();
    getNineB();
    getNineC();
    getNineD();
    // getNineE();
    getNineF();
}
