let timeoutId;
let timeoutset;

// 補0
function timeToString(number) {
    let numberString;
    if (number < 10) {
        numberString = `0${number.toString()}`;
    } else {
        numberString = number.toString();
    }
    return numberString;
}

function resetColorofClass(name, color) {
    const ydiv = document.getElementsByClassName(name);
    for (let i = 0; i < ydiv.length; i += 1) {
        ydiv[i].style.color = color;
    }
}


function startTime() {
    const today = new Date();
    let hh = today.getHours();
    let mm = today.getMinutes();
    let ss = today.getSeconds();
    hh = timeToString(hh);
    mm = timeToString(mm);
    ss = timeToString(ss);
    document.getElementById('clock-hour').innerHTML = `${hh}`;
    document.getElementById('clock-min').innerHTML = `${mm}`;
    document.getElementById('clock-sec').innerHTML = `${ss}`;
    timeoutId = setTimeout(startTime, 500);
}


function setZero() {
    // 字體顏色初始化
    resetColorofClass('clock-time', '#ffffff');
    resetColorofClass('clock-div', '#ffffff');
    let hh = 0;
    let mm = 0;
    let ss = 0;
    hh = timeToString(hh);
    mm = timeToString(mm);
    ss = timeToString(ss);
    document.getElementById('clock-hour').innerHTML = `${hh}`;
    document.getElementById('clock-min').innerHTML = `${mm}`;
    document.getElementById('clock-sec').innerHTML = `${ss}`;
    window.clearTimeout(timeoutId);
    window.clearTimeout(timeoutset);
}


function adjustTime(target, method) {
    // 字體顏色初始化
    resetColorofClass('clock-time', '#ffffff');
    resetColorofClass('clock-div', '#ffffff');
    window.clearTimeout(timeoutId);
    let hh = Number(document.getElementById('clock-hour').innerHTML);
    let mm = Number(document.getElementById('clock-min').innerHTML);
    let ss = Number(document.getElementById('clock-sec').innerHTML);

    switch (target) {
    case 'hour':
        hh = (method === 'add') ? (hh + 1) : (hh - 1);
        break;
    case 'min':
        mm = (method === 'add') ? (mm + 1) : (mm - 1);
        break;
    default:
        ss = (method === 'add') ? (ss + 1) : (ss - 1);
    }

    hh = (hh + 24) % 24;
    mm = (mm + 60) % 60;
    ss = (ss + 60) % 60;

    hh = timeToString(hh);
    mm = timeToString(mm);
    ss = timeToString(ss);

    document.getElementById('clock-hour').innerHTML = `${hh}`;
    document.getElementById('clock-min').innerHTML = `${mm}`;
    document.getElementById('clock-sec').innerHTML = `${ss}`;
    console.log(`${hh}:${mm}:${ss}`);
}

function countDown() {
    window.clearTimeout(timeoutId);
    const oneSec = 1000;
    const oneMin = oneSec * 60;
    const oneHour = oneMin * 60;
    const oneDay = oneHour * 24;

    const nowTime = new Date();
    nowTime.setHours(document.getElementById('clock-hour').innerHTML);
    nowTime.setMinutes(document.getElementById('clock-min').innerHTML);
    nowTime.setSeconds(document.getElementById('clock-sec').innerHTML);

    const targetTime = new Date();
    targetTime.setHours('0');
    targetTime.setMinutes('0');
    targetTime.setSeconds('0');

    let diff = nowTime.getTime() - targetTime.getTime();
    diff -= 1000;
    const numDay = Math.floor(diff / oneDay);
    diff -= numDay * oneDay;
    let hh = Math.floor(diff / oneHour);
    diff -= hh * oneHour;
    let mm = Math.floor(diff / oneMin);
    diff -= mm * oneMin;
    let ss = Math.ceil(diff / oneSec);
    hh = timeToString(hh);
    mm = timeToString(mm);
    ss = timeToString(ss);
    document.getElementById('clock-hour').innerHTML = `${hh}`;
    document.getElementById('clock-min').innerHTML = `${mm}`;
    document.getElementById('clock-sec').innerHTML = `${ss}`;
    timeoutset = setTimeout(countDown, 1000);
    console.log(diff);

    // 歸零處理
    if (diff === 0) {
        window.clearTimeout(timeoutset);
        // 變更顏色標記歸零
        resetColorofClass('clock-time', 'yellow');
        resetColorofClass('clock-div', 'yellow');
    }
}
